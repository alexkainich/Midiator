# Evalest Midiator

### C# Winforms application that allows the user to control the volume of windows application by using a MIDI controller.

The app allows the user to select from a list of available connected midi controllers.
Then by selecting an app and recording a midi control, the user can control the volume of this app.
Finally, all the assigned controls are saved into a .config file which can be shared between users.