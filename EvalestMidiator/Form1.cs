﻿using System;
using System.Data;
using System.Linq;
using System.Diagnostics;
using System.Windows.Forms;
using System.Threading;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using CSCore.CoreAudioAPI;
using NAudio.Midi;
using NAudio.Wave;
using NAudio.Mixer;
using System.IO;
using Microsoft.Win32;

namespace EvalestMidiator
{
    public partial class MidiController : Form
    {
        MidiIn midiIn;
        System.Windows.Forms.Timer timer1;
        public int recording = 0;
        public int AppOrMic = 0; //0: no selection, 1: App, 2: Mic
        public int noMidi = 0;
        public AudioSessionManager2 sessionManager;
        public AudioSessionEnumerator sessionEnumerator;
        public List<string> sessionNames = new List<string>();
        public List<int> sessionControls = new List<int>();
        public List<NAudio.CoreAudioApi.SimpleAudioVolume> sessionVolume = new List<NAudio.CoreAudioApi.SimpleAudioVolume>();
        public List<string> recordingDevices = new List<string>();
        public List<int> recordingControls = new List<int>();
        public string exePath;
        public NAudio.CoreAudioApi.AudioSessionManager sessionManager2;
        public NAudio.CoreAudioApi.MMDevice defaultdevice;

        public MidiController()
        {
            InitializeComponent();
        }

        private void MidiController_Load(object sender, EventArgs e)
        {
            SystemEvents.PowerModeChanged += OnPowerChange;

            alreadyRunning();
            findExePath();
            OnResize(EventArgs.Empty);
            WindowState = FormWindowState.Minimized;
            clearAllLists();

            activeApps.Items.Add("MasterVolume");
            sessionNames.Add("MasterVolume");
            sessionControls.Add(0);

            findSessions(); //find current open "with sound" apps
            findRecordingDevices();
            configureMidi();
            readConfigFile(); //read config file
            activeApps.Focus();
            startUpCheck();
            listenForSessions();
        }

        public void OnPowerChange(object s, PowerModeChangedEventArgs e)
        {
            Process self = Process.GetCurrentProcess();
            switch (e.Mode)
            {
                case PowerModes.Resume:
                    RestartApp(self.Id, "EvalestMidiator");
                    break;
                case PowerModes.Suspend:
                    break;
            }
        }

        static void RestartApp(int pid, string applicationName)
        {
            try
            {
                System.Diagnostics.Process.Start(System.Windows.Application.ResourceAssembly.Location);
                System.Windows.Application.Current.Shutdown();
            }
            catch
            {

            }
        }

        private void listenForSessions()
        {
            timer1 = new System.Windows.Forms.Timer();
            timer1.Tick += new EventHandler(reloadApps);
            timer1.Interval = 5000; // in miliseconds
            timer1.Start();
        }

        private void alreadyRunning()
        {
            Process self = Process.GetCurrentProcess();
            if (Process.GetProcessesByName("EvalestMidiator").Count() > 1)
            {
                foreach (var process in Process.GetProcessesByName("EvalestMidiator"))
                {
                    if (self.Id != process.Id)
                    {
                        process.Kill();
                    }
                }
            }
        }

        public void findSessions()
        {

            try
            {
                NAudio.CoreAudioApi.MMDeviceEnumerator devEnum = new NAudio.CoreAudioApi.MMDeviceEnumerator();
                defaultdevice = devEnum.GetDefaultAudioEndpoint(NAudio.CoreAudioApi.DataFlow.Render, NAudio.CoreAudioApi.Role.Multimedia);
                sessionManager2 = defaultdevice.AudioSessionManager;
            }
            catch
            {
                Console.WriteLine("Error in session manager !");
                return;
            }

            for (int i = 0; i < sessionManager2.Sessions.Count; i++)
            {
                try
                {
                    int procNum = (int)sessionManager2.Sessions[i].GetProcessID;
                    string tempName = Process.GetProcessById(procNum).ProcessName.ToString();
                    activeApps.Items.Add(tempName); //we populate the listview in the windows form
                    sessionNames.Add(tempName); //we save the session names in this list
                    sessionVolume.Add(sessionManager2.Sessions[i].SimpleAudioVolume);
                    sessionControls.Add(0);
                }
                catch
                {
                    Console.WriteLine("Error in adding the session. Maybe the session was closed while the method was running?");
                    continue;
                }
            }
        }

        public void findRecordingDevices()
        {
            int waveInDevices = WaveIn.DeviceCount;

            for (int waveInDevice = 0; waveInDevice < waveInDevices; waveInDevice++)
            {
                WaveInCapabilities deviceInfo = WaveIn.GetCapabilities(waveInDevice);
                activeMics.Items.Add(deviceInfo.ProductName);
                recordingDevices.Add(deviceInfo.ProductName);
                recordingControls.Add(0);
            }
        }

        public void setMicGain(int whichMic, double micGain)
        {
            int waveInDeviceNumber = whichMic; //0 is the Default
            var mixerLine = new MixerLine((IntPtr)waveInDeviceNumber, 0, MixerFlags.WaveIn);
            
            foreach (var control in mixerLine.Controls)
            {
                if (control.ControlType == MixerControlType.Volume)
                {
                    var volumeControl = control as UnsignedMixerControl;
                    volumeControl.Percent = micGain;
                    continue;
                }
                else if (control.ControlType == MixerControlType.Mute)
                {
                    var muteControl = control as BooleanMixerControl;
                    if (micGain == 0)
                    {
                        muteControl.Value = true;
                    }
                    else
                    {
                        muteControl.Value = false;
                    }
                    continue;
                }
            }
        }

        public void clearAllLists()
        {
            sessionNames.Clear();
            sessionControls.Clear();
            sessionVolume.Clear();
            recordingDevices.Clear();
            recordingControls.Clear();
        }

        public void configureMidi()
        {
            deviceList.Items.Clear();
            for (int device = 0; device < MidiIn.NumberOfDevices; device++) //list midi devices
            {
                deviceList.Items.Add(MidiIn.DeviceInfo(device).ProductName);
            }
            try
            {
                deviceList.SelectedIndex = 0;
                noMidi = 0;
            }
            catch
            {
                noMidi = 1;
                Console.WriteLine("No Midi device found!");
            }
        }

        public void readConfigFile()
        {
            string[] configFile = new string[0];
            int foundConfig;

            try
            {
                configFile = File.ReadAllLines(exePath + "\\volmix.config"); //open config file
                foundConfig = 1;
            }
            catch
            {
                Console.WriteLine("Warning: Config file not found");
                foundConfig = 0;
            }
            if (foundConfig == 1)
            {
                for (int i = 0; i < configFile.GetLength(0); i++)
                {
                    String[] substrings = configFile[i].Split(':'); //1: session name , 2: control
                    int StringToInt = 0;
                    Int32.TryParse(substrings[1], out StringToInt);
                    for (int j = 0; j < sessionNames.Count; j++)
                    {
                        if (substrings[0] == sessionNames[j])
                        {
                            sessionControls[j] = StringToInt;
                            activeApps.Items[j].SubItems.Add(sessionControls[j].ToString());
                        }
                    }
                    for (int j = 0; j < recordingDevices.Count; j++)
                    {
                        if (substrings[0] == recordingDevices[j])
                        {
                            recordingControls[j] = StringToInt;
                            activeMics.Items[j].SubItems.Add(recordingControls[j].ToString());
                        }
                    }
                }
            }
            for (int j = 0; j < sessionNames.Count; j++) //populate listview second column in case the config file didnt do it
            {
                if (activeApps.Items[j].SubItems.Count == 1)
                {
                    activeApps.Items[j].SubItems.Add("0");
                }
            }
            for (int j = 0; j < recordingDevices.Count; j++) //populate listview second column in case the config file didnt do it
            {
                if (activeMics.Items[j].SubItems.Count == 1)
                {
                    activeMics.Items[j].SubItems.Add("0");
                }
            }
        }

        public AudioSessionManager2 GetDefaultAudioSessionManager2(DataFlow dataFlow) //only used by the 'findSessions' method
        {
            using (var enumerator = new MMDeviceEnumerator())
            {
                using (var device = enumerator.GetDefaultAudioEndpoint(dataFlow, Role.Multimedia))
                {
                    var sessionManager = AudioSessionManager2.FromMMDevice(device);
                    return sessionManager;
                }
            }
        }

        private void deviceList_SelectedIndexChanged(object sender, EventArgs e) //starts the midi device, that the user selected in the drop down menu
        {
            try
            {
                if (midiIn != null) //stop current active midi device
                {
                    midiIn.Stop();
                    midiIn.Dispose();
                }
            }
            catch //midi is unplagged or may need restarting
            {
            }
            try
            {
                midiIn = new MidiIn(deviceList.SelectedIndex); //start new selected midi device
            }
            catch //midi is unplagged
            {
                return;
            }
            //midiIn.MessageReceived += midiIn_MessageReceived;
            midiIn.MessageReceived += new EventHandler<MidiInMessageEventArgs>(midiIn_MessageReceived);
            midiIn.Start();
            activeApps.Focus(); //so that we hide highlighting the selected device in the windows form
        }

        private void userSelection()
        {
            AppOrMic = 0;
            if (activeApps.SelectedIndices.Count > 0)
            {
                AppOrMic = 1;
            }
            else if (activeMics.SelectedIndices.Count > 0)
            {
                AppOrMic = 2;
            }
        }

        private void midiIn_MessageReceived(object sender, MidiInMessageEventArgs e)
        {
            Byte[] bytes = BitConverter.GetBytes(e.RawMessage); // 1: control number, 2: volume
            Console.WriteLine("Timestamp: " + e.Timestamp + ", volume: " + bytes[2]);
            try
            {
                if (recording == 1)
                {
                    if (AppOrMic == 0)
                    {
                        return;
                    }
                    else if (AppOrMic == 1)
                    {
                        Invoke((MethodInvoker)delegate ()
                        {
                            for (int i = 0; i < activeApps.Items.Count; i++)
                            {
                                if (activeApps.Items[i].Text == activeApps.SelectedItems[0].Text)
                                {
                                    activeApps.Items[i].SubItems[1].Text = (bytes[1] + 1).ToString();
                                    sessionControls[i] = (bytes[1] + 1);
                                    resultText.Text = "";
                                }
                            }
                        });
                    }
                    else if (AppOrMic == 2)
                    {
                        Invoke((MethodInvoker)delegate ()
                        {
                            for (int i = 0; i < activeMics.Items.Count; i++)
                            {
                                if (activeMics.Items[i].Text == activeMics.SelectedItems[0].Text)
                                {
                                    activeMics.Items[i].SubItems[1].Text = (bytes[1] + 1).ToString();
                                    recordingControls[i] = (bytes[1] + 1);
                                    resultText.Text = "";
                                }
                            }
                        });
                    }
                    recording = 0;
                }
                else
                {
                    var indices = Enumerable.Range(0, sessionControls.Count).Where(i => sessionControls[i] == (bytes[1] + 1)).ToList();
                    if (indices.Any()) //if list is not empty
                    {
                        foreach (var i in indices)
                        {
                            if (sessionNames[i] == "MasterVolume")
                            {
                                try
                                {
                                    defaultdevice.AudioEndpointVolume.MasterVolumeLevelScalar = bytes[2] / 127f;
                                }
                                catch
                                {
                                    Console.WriteLine("Error in setting the master volume!");
                                }
                            }
                            else
                            {
                                try
                                {
                                    sessionVolume[i-1].Volume = bytes[2] / 127f;
                                }
                                catch
                                {
                                    Console.WriteLine("Wrong session index");
                                }
                            }
                        }
                    }
                    var index = recordingControls.FindIndex(a => a == (bytes[1] + 1));
                    if (index != -1)
                    {
                        try
                        {
                            int tempor = (int)(((float)bytes[2] / 127) * 100);
                            setMicGain(index, tempor);
                        }
                        catch
                        {
                        }
                    }
                }
            }
            catch
            {
                Console.WriteLine("Error in index? App was reloading?");
            }
        }

        private void reloadApps(object sender, EventArgs e)
        {
            string tempNme = "";
            userSelection();
            if (AppOrMic == 1)
            {
                tempNme = activeApps.SelectedItems[0].Text;
            }
            else if (AppOrMic == 2)
            {
                tempNme = activeMics.SelectedItems[0].Text;
            }

            saveConfig();
            activeApps.Items.Clear();
            activeMics.Items.Clear();
            clearAllLists();
            findExePath();

            activeApps.Items.Add("MasterVolume");
            sessionNames.Add("MasterVolume");
            sessionControls.Add(0);

            findSessions(); //populate session names
            findRecordingDevices();
            configureMidi();
            readConfigFile(); //populate controls

            if (AppOrMic == 1)
            {
                foreach (ListViewItem item in activeApps.Items)
                {
                    if (item.Text == tempNme)
                    {
                        item.Selected = true;
                        break;
                    }

                }
            }
            else if (AppOrMic == 2)
            {
                foreach (ListViewItem item in activeMics.Items)
                {
                    if (item.Text == tempNme)
                    {
                        item.Selected = true;
                        break;
                    }

                }
            }
        }

        private void recordControl_Click(object sender, EventArgs e)
        {
            if (AppOrMic != 0)
            {
                resultText.Text = "Waiting for midi input ...";
                recording = 1;
            }
        }

        private void saveConfig()
        {
            string path = exePath + "\\volmix.config";
            if (!File.Exists(path))
            {
                var myFile = File.CreateText(path);
                myFile.Close();
            }

            string[] configFile = new string[0];
            List<string> newNames = new List<string>();
            List<int> newControls = new List<int>();
            int incri = 0;

            configFile = File.ReadAllLines(exePath + "\\volmix.config");
            for (int i = 0; i < configFile.GetLength(0); i++) //read file line by line
            {
                String[] substrings = configFile[i].Split(':'); //1: session name , 2: control
                if (substrings.Length != 2)
                {
                    Console.WriteLine("Error in the config file!");
                    continue;
                }
                else if (!sessionNames.Contains(substrings[0]) && !recordingDevices.Contains(substrings[0])) //check apps
                {
                    int StringToInt = 0;
                    Int32.TryParse(substrings[1], out StringToInt);
                    newNames.Add(substrings[0]);
                    newControls.Add(StringToInt);
                    incri++;
                }
            }

            File.WriteAllText(path, String.Empty);

            using (StreamWriter stwr = File.AppendText(path))
            {
                for (int i = 0; i < sessionNames.Count; i++)
                {
                    stwr.WriteLine(sessionNames[i] + ":" + sessionControls[i]);
                }
                for (int i = 0; i < recordingDevices.Count; i++)
                {
                    stwr.WriteLine(recordingDevices[i] + ":" + recordingControls[i]);
                }
                for (int i = 0; i < newControls.Count; i++)
                {
                    stwr.WriteLine(newNames[i] + ":" + newControls[i]);
                }
            }
        }

        private void RemoveControl_Click(object sender, EventArgs e)
        {
            if (activeApps.SelectedIndices.Count > 0)
            {
                activeApps.SelectedItems[0].SubItems[1].Text = "0";
                sessionControls[activeApps.SelectedItems[0].Index] = 0;
            }
            if (activeMics.SelectedIndices.Count > 0)
            {
                activeMics.SelectedItems[0].SubItems[1].Text = "0";
                recordingControls[activeMics.SelectedItems[0].Index] = 0;
            }
        }

        private void activeApps_MouseClick(object sender, EventArgs e)
        {
            if (activeMics.SelectedIndices.Count > 0)
            {
                activeMics.SelectedItems.Clear();
            }
            userSelection();
        }

        private void activeMics_MouseClick(object sender, EventArgs e)
        {
            if (activeApps.SelectedIndices.Count > 0)
            {
                activeApps.SelectedItems.Clear();
            }
            userSelection();
        }

        private void MidiController_Resize(object sender, EventArgs e)
        {
            if (FormWindowState.Minimized == WindowState)
            {
                Hide();
                notifyIcon.Visible = true;
                ShowInTaskbar = false;
            }
        }

        private void notifyIcon_MouseClick(object sender, EventArgs e)
        {
            Show();
            WindowState = FormWindowState.Normal;
        }

        private void checkStart_CheckedChanged(object sender, EventArgs e)
        {
            RegistryKey rk = Registry.CurrentUser.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run", true);
            if (checkStart.Checked)
            {
                rk.SetValue("EvalestMidiator", System.Windows.Forms.Application.ExecutablePath);
            }
            else
                rk.DeleteValue("EvalestMidiator", false);
        }

        private void findExePath()
        {
            RegistryKey rk = Registry.CurrentUser.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run", true);
            try
            {
                exePath = System.Reflection.Assembly.GetExecutingAssembly().Location.Replace("\\EvalestMidiator.exe", "");
            }
            catch
            {
                exePath = @".\";
            }
        }

        private void startUpCheck()
        {
            RegistryKey rk = Registry.CurrentUser.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run", true);
            if (rk.GetValue("EvalestMidiator") == null)
            {
                checkStart.Checked = false;
            }
            else
            {
                checkStart.Checked = true;
            }
        }

        private void onFormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                e.Cancel = true;
                WindowState = FormWindowState.Minimized;
                Hide();
                notifyIcon.Visible = true;
                ShowInTaskbar = false;
            }
        }

        private void closeButton_Click(object sender, EventArgs e)
        {
            System.Windows.Forms.Application.Exit();
        }

    }
}
