﻿namespace EvalestMidiator
{
    partial class MidiController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MidiController));
            this.recordControl = new System.Windows.Forms.Button();
            this.RemoveControl = new System.Windows.Forms.Button();
            this.deviceList = new System.Windows.Forms.ComboBox();
            this.notifyIcon = new System.Windows.Forms.NotifyIcon(this.components);
            this.activeMics = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.activeApps = new System.Windows.Forms.ListView();
            this.Application = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.MidiControl = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.checkStart = new System.Windows.Forms.CheckBox();
            this.resultText = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.closeButton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // recordControl
            // 
            this.recordControl.Location = new System.Drawing.Point(12, 129);
            this.recordControl.Name = "recordControl";
            this.recordControl.Size = new System.Drawing.Size(151, 30);
            this.recordControl.TabIndex = 4;
            this.recordControl.Text = "Record Control";
            this.recordControl.UseVisualStyleBackColor = true;
            this.recordControl.Click += new System.EventHandler(this.recordControl_Click);
            // 
            // RemoveControl
            // 
            this.RemoveControl.Location = new System.Drawing.Point(12, 165);
            this.RemoveControl.Name = "RemoveControl";
            this.RemoveControl.Size = new System.Drawing.Size(151, 30);
            this.RemoveControl.TabIndex = 5;
            this.RemoveControl.Text = "Remove Control";
            this.RemoveControl.UseVisualStyleBackColor = true;
            this.RemoveControl.Click += new System.EventHandler(this.RemoveControl_Click);
            // 
            // deviceList
            // 
            this.deviceList.FormattingEnabled = true;
            this.deviceList.Location = new System.Drawing.Point(12, 102);
            this.deviceList.Name = "deviceList";
            this.deviceList.Size = new System.Drawing.Size(151, 21);
            this.deviceList.TabIndex = 6;
            this.deviceList.SelectedIndexChanged += new System.EventHandler(this.deviceList_SelectedIndexChanged);
            // 
            // notifyIcon
            // 
            this.notifyIcon.Icon = ((System.Drawing.Icon)(resources.GetObject("notifyIcon.Icon")));
            this.notifyIcon.Text = "Evalest - Midiator";
            this.notifyIcon.MouseClick += new System.Windows.Forms.MouseEventHandler(this.notifyIcon_MouseClick);
            // 
            // activeMics
            // 
            this.activeMics.BackColor = System.Drawing.SystemColors.Window;
            this.activeMics.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2});
            this.activeMics.GridLines = true;
            this.activeMics.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.activeMics.HideSelection = false;
            this.activeMics.Location = new System.Drawing.Point(182, 223);
            this.activeMics.Name = "activeMics";
            this.activeMics.Size = new System.Drawing.Size(290, 110);
            this.activeMics.TabIndex = 11;
            this.activeMics.UseCompatibleStateImageBehavior = false;
            this.activeMics.View = System.Windows.Forms.View.Details;
            this.activeMics.MouseClick += new System.Windows.Forms.MouseEventHandler(this.activeMics_MouseClick);
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Recording Devices";
            this.columnHeader1.Width = 230;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Midi";
            this.columnHeader2.Width = 50;
            // 
            // activeApps
            // 
            this.activeApps.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.Application,
            this.MidiControl});
            this.activeApps.GridLines = true;
            this.activeApps.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.activeApps.HideSelection = false;
            this.activeApps.Location = new System.Drawing.Point(182, 12);
            this.activeApps.Name = "activeApps";
            this.activeApps.Size = new System.Drawing.Size(290, 200);
            this.activeApps.TabIndex = 9;
            this.activeApps.UseCompatibleStateImageBehavior = false;
            this.activeApps.View = System.Windows.Forms.View.Details;
            this.activeApps.MouseClick += new System.Windows.Forms.MouseEventHandler(this.activeApps_MouseClick);
            // 
            // Application
            // 
            this.Application.Text = "Application";
            this.Application.Width = 230;
            // 
            // MidiControl
            // 
            this.MidiControl.Text = "Midi";
            this.MidiControl.Width = 50;
            // 
            // checkStart
            // 
            this.checkStart.AutoSize = true;
            this.checkStart.Location = new System.Drawing.Point(15, 272);
            this.checkStart.Name = "checkStart";
            this.checkStart.Size = new System.Drawing.Size(98, 17);
            this.checkStart.TabIndex = 12;
            this.checkStart.Text = "Start on startup";
            this.checkStart.UseVisualStyleBackColor = true;
            this.checkStart.CheckedChanged += new System.EventHandler(this.checkStart_CheckedChanged);
            // 
            // resultText
            // 
            this.resultText.AutoSize = true;
            this.resultText.Location = new System.Drawing.Point(12, 199);
            this.resultText.Name = "resultText";
            this.resultText.Size = new System.Drawing.Size(0, 13);
            this.resultText.TabIndex = 13;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox1.InitialImage")));
            this.pictureBox1.Location = new System.Drawing.Point(2, 12);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(180, 70);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 14;
            this.pictureBox1.TabStop = false;
            // 
            // closeButton
            // 
            this.closeButton.BackColor = System.Drawing.Color.DarkRed;
            this.closeButton.ForeColor = System.Drawing.Color.White;
            this.closeButton.Location = new System.Drawing.Point(12, 295);
            this.closeButton.Name = "closeButton";
            this.closeButton.Size = new System.Drawing.Size(151, 38);
            this.closeButton.TabIndex = 15;
            this.closeButton.Text = "Close";
            this.closeButton.UseVisualStyleBackColor = false;
            this.closeButton.Click += new System.EventHandler(this.closeButton_Click);
            // 
            // MidiController
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.ClientSize = new System.Drawing.Size(484, 341);
            this.Controls.Add(this.closeButton);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.resultText);
            this.Controls.Add(this.checkStart);
            this.Controls.Add(this.activeMics);
            this.Controls.Add(this.activeApps);
            this.Controls.Add(this.deviceList);
            this.Controls.Add(this.RemoveControl);
            this.Controls.Add(this.recordControl);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(500, 380);
            this.MinimumSize = new System.Drawing.Size(500, 380);
            this.Name = "MidiController";
            this.Text = "Evalest - Midiator";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.onFormClosing);
            this.Load += new System.EventHandler(this.MidiController_Load);
            this.Resize += new System.EventHandler(this.MidiController_Resize);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button recordControl;
        private System.Windows.Forms.Button RemoveControl;
        private System.Windows.Forms.ComboBox deviceList;
        private System.Windows.Forms.NotifyIcon notifyIcon;
        private System.Windows.Forms.ListView activeMics;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ListView activeApps;
        private System.Windows.Forms.ColumnHeader Application;
        private System.Windows.Forms.ColumnHeader MidiControl;
        private System.Windows.Forms.CheckBox checkStart;
        private System.Windows.Forms.Label resultText;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button closeButton;
    }
}

